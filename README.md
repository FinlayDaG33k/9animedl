# DISCONTINUED
9AnimeDl is no longer maintained.  
You can read the announcement on this [here](https://www.reddit.com/r/FinlayDaG33k/comments/j17u4u/9animedl_so_long_my_friend/), as well as find a replacement [here](https://www.reddit.com/r/FinlayDaG33k/comments/j4e6p2/gogodl_gogodl_version_010_has_been_released/).

# 9AnimeDl
Tool to download anime of 9AnimeDl.  
Useful for when you want to take the anime on the go (eg. in a car or plane) or when your connection may not be the best for streaming.  

# Downloading pre-built binaries
Pre-build binaries will be released on my website:
https://www.finlaydag33k.nl/projects/1

# Build from source
Below, you can find general instructions on how to build 9AnimeDl on your local machine for development and distribution purposes.  
All three depend on `yarn` so please, do make sure you have that installed.  

First, clone 9AnimeDl and install it's dependencies:
```
git clone https://gitlab.com/finlaydag33k/9animedl.git
cd 9animedl
yarn install
```

Then, after that is all done, you can start 9AnimeDl in development mode.  
This includes the default electron frame to have an easier time moving around, it will not be present in the main distributions.  
```
yarn start
```

In order to build the app in production mode, you can build it using the command below.  
While in production mode, the electron frame is removed, access to the console is retained when requested (`crtl + shift + I`).  
Currently, the app supports building for 3 different platforms:
- Windows
- Linux (FlatPak)
- MacOS

## Windows
```
yarn run electron:windows
```

## Linux (FlatPak)
```
yarn run electron:linux
```

## MacOS
```
yarn run electron:mac
```

# Testing
I literally have spend no time to build unit tests.  
If you really want to do this, feel free to build the unit tests and open a pull request.

# Thanks lads!
Because I think it's only fair people get attribution for the work they have delivered, I have a section dedicated to you guys!  
Without the people below, this project would either have not existed or probably be even more unstable than it already is.  

## General thanks
First of all, I want to thank 9Anime for delivering high-quality anime to us for free!  
This project would not have existed without you in the first place!

Second of all, I want to bring out a special thanks to the folks below that helped me out a lot in the project as well in a variety of different ways.  

- Spike2147 (Helping me out with loads of code issues)
- Blake S. (Loyal bug reporter)
- Apollo (Mental support)

Third of all, I want to thank all the contributors of major dependencies used by the project.  
The work of these folks have made making this project a lot easier.  
Please note that these dependencies might also have their own dependencies and as such, I also wish to shout-out to the folks who made this project possible indirectly!  
The list below is in no particular order :)  

- [ElectronJS](https://electronjs.org/)
- [Angular](https://angular.io/)
- [Bootstrap](https://getbootstrap.com/)
- [Puppeteer](https://pptr.dev/)
- [jQuery](https://jquery.com/)
- [NodeJS](https://nodejs.org/en/)
- [Moment](https://momentjs.com/)

If you're not into the list but have made a contribution to the project, don't worry, I have definitely not forgotten about you!  
I probably just didn't have time to add you to the list yet :)

## Financial support
The people below have provided financial support to the project by sending anything you can miss to the following places.  
Donations made will be listed here publically :)

- [PayPal](https://www.paypal.me/finlaydag33k)
- [Bitcoin](bitcoin:17f77AYHsQbdsB1Q6BbqPahJ8ZrjFLYH2j)
- [Bitcoin Cash](bitcoincash:17f77AYHsQbdsB1Q6BbqPahJ8ZrjFLYH2j)
- Ethereum: TBA (feel free to ask) :)
- More cryptocurrencies available (feel free to ask) :)

This project is built in my own time next to my regular day-job.  
The financial support offered by these people may not be significant but any contribution but in The Netherlands, we have a saying: "alle kleine beetjes helpen" ("all small bits help").  
With the financial support I recieved, I not only help fund this project and my other projects, it also allows me to get back on the rails if something happens to my gear (eg. my harddrive breaks).  


### Donations recieved
- S. Gildersleeve ($20,00)
- K. Monrroy ($10,00)
- B. Boberg ($5,00)
- Z. Kroening ($5,00)
- Frozenfrostfire (&euro; 5,00)
- R. Van Driessche (&euro; 5,00)
- Z. Ismayatim (&euro; 3.65)

# A note on my code
This project is a "learn-as-I-go" project.  
This means that the code might not be what a more seasoned developer would like to see and that there might be plenty of bugs in the long run.  
If you are a more seasoned developer, feel free to open a PR or leave feedback so I can improve the code :)  
Thanks for your understanding <3