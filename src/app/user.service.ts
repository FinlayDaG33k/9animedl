import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Time } from './util/Time.util';
import { StatusHandler } from './util/StatusHandler';
import { Notifier } from './util/Notifier.util';
import { _9Anime } from './util/_9Anime.util';

export interface User {
  loggedin: boolean,
  id?: number,
  username?: string,
  email?: string,
  fullname?: string
}

@Injectable({
  providedIn: 'root'
})

export class UserService {
  user: User = {
    loggedin: false
  };

  loginError = "";
  time = new Time();
  statusHandler = new StatusHandler();

  constructor(
    private httpClient: HttpClient,
    private notifier: Notifier,
    private _9anime: _9Anime
  ){}

  public async login(loginData) {
    const params = new HttpParams({
      fromObject: loginData
    }).toString();

    let resp = await this.httpClient.post(
      `https://${this._9anime._ApiDomain}.${this._9anime._9AnimeHost}/user/ajax/login`, 
      params,
      {
        responseType: 'json',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        withCredentials: true
      }
    );

    return resp.toPromise();
  }

  public async getDetails() {
    let resp = await this.httpClient.get(
      `https://9anime.to/user/ajax/menu-bar?ts=${this.time.getUnixTimestamp(true)}&_=684`,
      {
        observe: 'response'
      }
    );
    let data = await resp.toPromise();

    if(!data.body['user']){
      return null;
    }

    let user = data.body['user'];
    this.user.loggedin = true;
    this.user.id = user.user_id || "";
    this.user.username = user.username || "";
    this.user.email = user.email || "";
    this.user.fullname = user.fullname || "";
  }

  public async logout() {
    this.user = {
      loggedin: false
    };

    let resp = await this.httpClient.get(
      `https://9anime.to/user/logout`   
    ).toPromise().catch(err => {});
  }
}
