import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  appVersion;
  repoUrl = 'https://gitlab.com/finlaydag33k/9animel';

  constructor(
    public appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.appVersion = require('electron').remote.app.getVersion();
  }

}
