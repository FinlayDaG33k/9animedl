import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components';
import { AboutComponent } from './about/about.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import { RecentlyDownloadedComponent } from './recently-downloaded/recently-downloaded.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'discover',
    pathMatch: 'full'
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'watchlist',
    component: WatchlistComponent
  },
  {
    path: 'recently-downloaded',
    component: RecentlyDownloadedComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { 
      useHash: true,
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
