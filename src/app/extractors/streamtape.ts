import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class StreamtapeExtractor {
  public async doExtract(embed) {
    // Get the HTML from the page
    // Then parse it into a DOM so we can work with it
    let resp = await fetch(embed);
    let html = await resp.text();
    var parser = new DOMParser();
    var doc = parser.parseFromString(html, 'text/html');

    // Obtain the video link
    let el = <HTMLElement>doc.querySelector(`div#videolink`);
    let link = el.innerText;
    
    // Return our link (along with an https prefix)
    return `https:${link}`;
  }
}