import { Injectable } from '@angular/core';
import { promises } from 'fs';

@Injectable({
  providedIn: 'root'
})

export class Mp4UploadExtractor {
  puppeteer = window.require('puppeteer');
  video = "";

  public async doExtract(embed) {
    let self = this;
    const browser = await this.puppeteer.launch({
      headless: true,
      executablePath: this.puppeteer.executablePath().replace('app.asar', 'app.asar.unpacked')
    });
    const page = await browser.newPage();

    await page.setRequestInterception(true);

    // Event for when the video itself loaded up
    page.on('request', function(request) {
      if (request.url().includes('video.mp4')) {
        self.video = request.url();
      } else {
        request.continue();
      }
    });

    // Navigate to the embed
    await page.goto(embed);

    // Wait till the video element has loaded up
    await page.evaluate(() => {
      const el = <HTMLElement>document.querySelector(`div#player div.cover`);
      el && el.click();
    });

    // Wait a second before closing the browser
    await page.waitFor(1000);
    await browser.close();

    return this.video;
  }
}