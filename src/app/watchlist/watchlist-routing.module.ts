import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { WatchlistComponent } from './watchlist.component';

const routes: Routes = [
  {
    path: 'watchlist',
    component: WatchlistComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WatchlistRoutingModule {}
