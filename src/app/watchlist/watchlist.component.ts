import { Component, OnInit } from '@angular/core';
import { _9Anime } from '../util/_9Anime.util';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.scss']
})

export class WatchlistComponent {
  series = [];

  constructor(
    private _9anime: _9Anime,
    private appComponent: AppComponent
  ) {
    this.appComponent.isBusy = true;
    this._9anime.getWatchlist().then((res) => {
      this.series = this.series.concat(res);
      this.appComponent.isBusy = false;
    });
  }
}
