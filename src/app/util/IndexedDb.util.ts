import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class IndexedDb {
  db;
  
  public async open(db: string, version: number, upgradeCallback) {
    let openDb = new Promise((resolve, reject) => {
      let request = window.indexedDB.open(db, version);
      request.onupgradeneeded = upgradeCallback;
      request.onsuccess = () => resolve(request.result);
      request.onerror = () => reject(request.error);
      request.onblocked = () => { console.log('blocked'); };
    });

    this.db = await openDb;
  }

  public getDb() {
    return this.db;
  }

  public insert(table: string, entry){
    let self = this;
    return new Promise((resolve, reject) => {
      let request = self.db
        .transaction([table], "readwrite")
        .objectStore(table)
        .add(entry);
      request.onsuccess = () => resolve(request.result);
      request.onerror = () => reject(request.error);
      request.onblocked = () => { console.log('blocked'); };
    });
  }

  public get(table: string, key: string = null): Promise<Array<Object>> {
    let self = this;
    return new Promise((resolve, reject) => {
      let request;
      if(key) {
        request = self.db
          .transaction(table)
          .objectStore(table)
          .get(key);
      } else {
        request = self.db
          .transaction(table)
          .objectStore(table)
          .getAll();
      }

      request.onsuccess = () => resolve(request.result);
      request.onerror = () => reject(request.error);
      request.onblocked = () => { console.log('blocked'); };
    });
  }
}