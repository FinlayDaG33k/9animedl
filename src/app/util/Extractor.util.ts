import { Injectable } from '@angular/core';

import { _9Anime } from '../util/_9Anime.util';

import { Mp4UploadExtractor } from '../extractors/mp4upload';
import { StreamtapeExtractor } from '../extractors/streamtape';

@Injectable({
  providedIn: 'root'
})

export class Extractor {
  classes = {
    Mp4UploadExtractor,
    StreamtapeExtractor
  }

  constructor(
    private _9anime: _9Anime
  ){}

  public async extract(opts) {
    // Define variable for the class name
    let serverName = Object.keys(this._9anime.servers).find(key => this._9anime.servers[key] === opts.server);

    // Instanciate the class
    let extractorClass = new this.classes[`${serverName}Extractor`]();

    // Extract the url
    try {
      let video = await extractorClass.doExtract(opts.url);
      return video;
    } catch (e) {
      return null;
    }
  }
}