import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { Waf } from './Waf.util';
import { Time } from './Time.util';
import { Logger } from './Logger.util';
import { Cache } from './Cache.util';
import { throwError } from 'rxjs';
import { StatusHandler } from './StatusHandler';

@Injectable({
  providedIn: 'root'
})

export class _9Anime {
  public htmlParser = new DOMParser();
  public waf = new Waf(this.httpClient, this);
  public isWafActive = false;
  public siteKey = "";
  public time = new Time();
  public _9AnimeHost = "9anime.to";
  public _ApiDomain = "www12";
  public servers = {
    "Mp4Upload": 35,
    "F5 - HQ": 36,
    "Streamtape": 40
  };
  private logger = new Logger();
  private cache = new Cache();
  public isBanned = false;
  private statusHandler = new StatusHandler();

  constructor(
    private httpClient: HttpClient
  ) {
    // Update the host and subdomain if need be
    fetch(`https://${this._ApiDomain}.${this._9AnimeHost}/`, { method: 'GET'})
      .then(response => {
        if(response.redirected) {
          this.logger.info(`Found change in host: ${response.url}`)
          const regex = /https:\/\/([w0-9]+)\.9anime.([a-z]+)\//;
          const matches = response.url.match(regex);
          this._ApiDomain = matches[1];
          this._9AnimeHost = `9anime.${matches[2]}`;
        }
      });
  }

  public async getHttpContent(url, isWafCheck: boolean = false): Promise<any> {
    // Check whether this request is for the waf check
    if(!isWafCheck) {
      // We're not checking for the waf already
      // Check if the waf is active
      this.isWafActive = await this.waf.checkWaf();
      if(this.isWafActive) {
        // Wait for the waf to be solved by the user
        this.logger.warning('WAF detected!');
        await this.waf.waitForWaf(this.isWafActive);
        this.logger.info('WAF is no longer holding us up!');
      }
    }

    if(isWafCheck) {
      this.logger.info(`Sending request to ${url} for WAF check`);
    } else {
      this.logger.info(`Sending request to ${url}`);
    }
    

    let resp = this.httpClient.get(
      url, {
        responseType: 'text',
        observe: 'response'
      }
    );


    let prom = resp.toPromise();
    // Check the response to see whether we have been banned
    this.isBanned = await this.banCheck(await prom);
    if(this.isBanned) this.logger.warning('Detected a ban!');

    return prom;
  }

  public async discoverAnime(page: number = 1): Promise<any> {
    // Create a new array
    let seriesData = [];

    // Check if we have a cached version
    if(this.cache.check(`discovery cache page=${page}`)) {
      let cache = this.cache.get(`discovery cache page=${page}`)
      if(!cache) return { error: true, internal: true, message: `Could not obtain cache item "discovery cache page=${page}"`};
      return { error: false, data: cache };
    }

    // Get the content of the homepage
    let res = await this.getHttpContent(`https://${this._ApiDomain}.${this._9AnimeHost}/updated?page=${page}`);

    // Check our status
    let status = this.statusHandler.handle(res);
    if(status.error) return { error: true, internal: status.internal, message: status.message};

    // Turn the content into a DOM node
    var html = this.htmlParser.parseFromString(res.body, "text/html");

    // Extract all the series from the DOM node
    let series = html.querySelectorAll("div.widget div.widget-body div.film-list div.item div.inner");

    // Loop over all the series
    series.forEach(serie => {
      // Get the HTML content for the current serie
      let innerHtml = this.htmlParser.parseFromString(serie.innerHTML, "text/html");

      // Get the episodes label
      let episodeElement = innerHtml.querySelector("div.ep") ||
                           innerHtml.querySelector("div.special") ||
                           innerHtml.querySelector("div.movie") ||
                           innerHtml.querySelector("div.ova") ||
                           innerHtml.querySelector("div.ona") ||
                           innerHtml.querySelector("div.preview");
                           
      // Clean up the episode label a bit
      let episodeLabel = "N/A";
      if(episodeElement.textContent) episodeLabel = episodeElement.textContent.trim();
      else if(episodeElement.nodeValue) episodeLabel = episodeElement.nodeValue.trim();

      
      // Add the serie to the data
      seriesData.push({
        name: this.getAnimeName(innerHtml),
        url: this.getAnimeUrl(innerHtml),
        thumbnail: this.getAnimeThumb(innerHtml),
        episodes: episodeLabel 
      });
    });

    // Update our cache
    this.cache.set(`discovery cache page=${page}`, seriesData);

    // Return the series data
    return { error: false, data: seriesData };
  }

  public async searchAnime(query: string = ''): Promise<any> {
    let seriesData = [];

    // Get the content of the search page
    let res = await this.getHttpContent(`https://${this._9AnimeHost}/search?keyword=${query}`);

    // Check our status
    let status = this.statusHandler.handle(res);
    if(status.error) return { error: true, internal: status.internal, message: status.message};

    let html = this.htmlParser.parseFromString(res.body, "text/html");
    let series = html.querySelectorAll("div.film-list div.item div.inner");

    series.forEach(serie => {
      let innerHtml = this.htmlParser.parseFromString(serie.innerHTML, "text/html");
      // Get the episodes label
      let episodeElement = innerHtml.querySelector("div.ep") ||
                           innerHtml.querySelector("div.special") ||
                           innerHtml.querySelector("div.movie") ||
                           innerHtml.querySelector("div.ova") ||
                           innerHtml.querySelector("div.ona") ||
                           innerHtml.querySelector("div.preview");
                           
      // Clean up the episode label a bit
      let episodeLabel = "N/A";
      if(episodeElement) {
        if(episodeElement.textContent) episodeLabel = episodeElement.textContent.trim();
        else if(episodeElement.nodeValue) episodeLabel = episodeElement.nodeValue.trim();
      }
      

      
      // Add the serie to the data
      seriesData.push({
        name: this.getAnimeName(innerHtml),
        url: this.getAnimeUrl(innerHtml),
        thumbnail: this.getAnimeThumb(innerHtml),
        episodes: episodeLabel 
      });
    });
    
    return { error: false, data: seriesData };
  }

  public getAnimeId(url: string) {
    let regex = /.+\.([a-z0-9]+)/;
    try {
      let res = url.match(regex);
      return res[1];
    } catch (e) {
      return null;
    }
  }

  private getAnimeName(html: Document): string {
    // Search by name attribute
    if(html.querySelector("a.name"))
      return html.querySelector("a.name").textContent.trim();

    // Search by data-jtitle
    if(html.querySelector("a[data-jtitle]").getAttribute("data-jtitle"))
      return html.querySelector("a[data-jtitle]").getAttribute("data-jtitle").trim();
    
    // We haven't found anything, return questionmarks
    return "???";
  }

  private getAnimeUrl(html: Document): string {
    // get by href
    if(html.querySelector("a[data-jtitle]").getAttribute("href"))
      return html.querySelector("a[data-jtitle]").getAttribute("href").trim();

    // We haven't found anything, return empty string
    return "";
  }

  private getAnimeThumb(html: Document): string {
    // Get lazy loaded images
    if(html.querySelector("img").getAttribute("data-src"))
      return 'https://' + html.querySelector("img").getAttribute("data-src").trim();

    // Get regular images
    if(html.querySelector("img").getAttribute("src"))
      return 'https://' + html.querySelector("img").getAttribute("src").trim();

    // We haven't found anything, return empty string
    return "";
  }

  public getServerId(name: string): number {
    return this.servers[name];
  }

  public getServerName(id: number): string {
    return Object.keys(this.servers).find(key => this.servers[key] === id);
  }

  public async getEpisodesList(data: Document, serverId: number) {
    // Create an array that will hold the episodes list
    let episodesList = [];

    // Get the episodes for the specified serverId
    let links = data.querySelectorAll(`div[data-name="${serverId}"] li>a`);

    // Loop through all the episodes found for the mirror
    links.forEach(episode => {
      // Check if the current episode if uncensored or not
      // This code has not been tested (just a plain re-write from the legacy 9AnimeDl)
      // TODO: Find anime with both censored and uncensored episodes to test
      if(!episode.getAttribute("data-comment").includes("uncen")) {
        // The current episode does not seem to be uncensored
        // Check if an uncensored version is available
        let uncensored = this.getUncensoredEpisode(episode.getAttribute("data-comment"), serverId, data);

        if(uncensored != "") {
          // Uncensored version has been found
          // Add the uncensored version to the episodes list instead
          episodesList.push({
            episode: episode.getAttribute("data-base"),
            id: uncensored,
            server: serverId
          });
        } else {
          // No uncensored version was found
          // Just add the censored version :(
          episodesList.push({
            episode: episode.getAttribute("data-base"),
            id: episode.getAttribute("data-id"),
            server: serverId
          });
        }
      }
    });

    return episodesList;
  }

  private getUncensoredEpisode(episode: string, serverId: number, data: Document) {
    // Select all uncensored links for this episode and server
    // Should (in theory) only have one though
    let uncensoredLinks = document.querySelectorAll(`div[data-name="${serverId}"] li>a[data-comment$="${episode}uncen"]`);

    // Check if a result has been found
    if(uncensoredLinks.length >=1) {
      // A result has been found
      // Get the episode id and return it
      return uncensoredLinks[0].getAttribute("data-id");
    }

    // No uncensored link has been found
    // Return an empty string
    return "";
  }

  public async getEmbed(opts) {
    // Build the episodeUrl
    let episodeUrl = `https://${this._ApiDomain}.${this._9AnimeHost}/ajax/episode/info?ts=${this.time.getUnixTimestamp(true)}&_=936&id=${opts.id}&mcloud=6b07b&server=${opts.server}`;
    this.logger.debug(`episode url: ${episodeUrl}`);

    // Get the JSON
    let resp = await this.getHttpContent(episodeUrl);
    
    // Check our status
    let status = this.statusHandler.handle(resp);
    if(status.error) return { error: true, internal: status.internal, message: status.message};

    // Get the target
    let data = JSON.parse(resp.body);
    let embed = data.target;
    this.logger.debug(`Embed URL: ${embed}`);

    // Return the result
    return { error: false, url: embed };
  }

  public async getWatchlist() {
    // Check if we have a cached version
    if(this.cache.check(`watchlist`)) {
      return this.cache.get(`watchlist`);
    }

    let watchlist = [];

    // Get our watchlist content
    let res = await this.getHttpContent(`https://${this._ApiDomain}.${this._9AnimeHost}/user/watchlist?folder=all&all-page=1`);

    // Check our status
    let status = this.statusHandler.handle(res);
    if(status.error) return { error: true, internal: status.internal, message: status.message};

    // Parse the response
    let html = this.htmlParser.parseFromString(res, "text/html");
    let series = html.querySelectorAll("div.widget.watchlist div.widget-body div.items div.item");

    // Create our watchlist entry
    series.forEach(serie => {
      // Get the name
      let name;
      try {
        name = serie.querySelector("a.link").textContent.trim();
      } catch (e) {
        throwError(e);
        name = "???";
      }

      // Get the thumbnail
      let thumbnail;
      try {
        thumbnail = serie.querySelector('img').getAttribute('src').trim();
      } catch (e) {
        throwError(e);
        thumbnail = "";
      }
 
      // Get the url
      let url;
      try {
        url = serie.querySelector("a.link").getAttribute("href").trim();
      } catch (e) {
        throwError(e);
        url = "";
      }

      // Get the episodes
      let episodes;
      try {
        episodes = serie.querySelector("div.info span.status").textContent.trim();
      } catch (e) {
        throwError(e);
        episodes = "";
      }

      // Get the status
      let status;
      try {
        status = serie.classList[1];
      } catch (e) {
        throwError(e);
        status = "";
      }

      watchlist.push({
        name:  name,
        thumbnail: thumbnail,
        url: url,
        episodes: episodes,
        status: status
      });
    });
    
    // Set the cache
    this.cache.set('watchlist', watchlist, 1);

    return watchlist;
  }

  public async banCheck(data: any): Promise<boolean> {
    try {
      let html = this.htmlParser.parseFromString(data.body, "text/html");
      let title = html.querySelectorAll("title")[0].innerHTML;
      return title == 'BANNED';
    } catch(e) {
      return false;
    }
  }
}