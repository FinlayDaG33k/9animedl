import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class FNFormat {
  public format(anime, season, episode) {
    return `${anime} - S${season}E${episode}`;
  }
}