import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class Time {
  public getUnixTimestamp(trunc: boolean = false) {
    let stamp = Date.now() / 1000;

    // Truncate if we've set the flag for it
    if(trunc) stamp = Math.trunc(stamp);

    return stamp;
  }

  public addMinutes(minutes: number) {
    return (Date.now() + (minutes * 60_000)) / 1000;
  }
}