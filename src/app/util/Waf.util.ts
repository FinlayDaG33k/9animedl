import { Injectable } from '@angular/core';
import { _9Anime } from './_9Anime.util';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

const EventEmitter = require('events');

@Injectable({
  providedIn: 'root'
})

export class Waf {
  session = require('electron').remote.session;

  constructor(
    private httpClient: HttpClient,
    private _9anime: _9Anime
  ){}

  public async checkWaf() {
    // Get the content of the page and parse it
    let res = await this._9anime.getHttpContent(`https://${this._9anime._ApiDomain}.${this._9anime._9AnimeHost}/anime`, true);
    let html = this._9anime.htmlParser.parseFromString(res.body, "text/html");

    // Get the title of the page
    let title = html.getElementsByTagName("title");

    // Return whether the title of the page is "WAF"
    if(title[0].textContent != "WAF") {
      return false;
    }

    // Obtain the sitekey
    this._9anime.siteKey = html.getElementsByClassName("g-recaptcha")[0].getAttribute('data-sitekey');
    return true;
  }

  public async submitWaf(token) {
    let body = new HttpParams()
    .set('g-recaptcha-response', token);

    let resp = await this.httpClient.post(
      `https://${this._9anime._ApiDomain}.${this._9anime._9AnimeHost}/waf-verify`,
        body.toString(), 
        {
          responseType: 'text',
          headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        }
    ).toPromise();

    if(await this.getWafToken) {
      this._9anime.isWafActive = false;
      return true;
    }
  }

  private async getWafToken(): Promise<string> {
    let cookies = await this.session.defaultSession.cookies.get({name: 'waf_token'});
    if(typeof(cookies[0]) === "undefined") {
      return null;
    }

    return cookies[0].value;
  }

  public waitForWaf(isWafActive) {
    // Pass the _9Anime class
    let _9anime = this._9anime;

    // Create and return a new promise
    return new Promise(resolve => {
      // function to check whether the Waf is active
      // according to the flag that is
      function checkIsActive() {
        if (_9anime.isWafActive === false) {
          // Waf is not active, resolve!
          resolve();
        } else {
          // Waf is active, wait another second
          window.setTimeout(checkIsActive, 1000); 
        }
      }
      checkIsActive();
    });
  }
}