import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-anime-card',
  templateUrl: './anime-card.component.html',
  styleUrls: ['./anime-card.component.scss']
})
export class AnimeCardComponent implements OnInit {
  @Input() serie: any;

  constructor(
    private router: Router,
    private appComponent: AppComponent
  ) { }

  ngOnInit() {
  }

  public selectSerie(serie) {
    this.appComponent.isBusy = true;

    // Check if the download already exists
    let download = this.appComponent.downloadTabService.searchDownload(serie.name);
    if(download != -1) {
      // Download already exists, use it instead
      this.router.navigate(['download'], { 
        state: { 
          index: download
        }
      });
      return;
    }

    // Download doesn't already exist
    // Create it
    const index = this.appComponent.downloadTabService.addDownload(serie);
    this.router.navigate(['download'], { 
      state: { 
        index: index
      } 
    });
  }
}
