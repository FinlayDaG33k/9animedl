import { Component, AfterViewInit, Input, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Waf } from '../../../util/Waf.util';
import { _9Anime } from '../../../util/_9Anime.util';
import { BrowserWindow, BrowserView } from 'electron';

@Component({
  selector: 'app-waf-captcha',
  templateUrl: './waf-captcha.component.html',
  styleUrls: ['./waf-captcha.component.scss']
})


export class WafCaptchaComponent implements AfterViewInit {
  @Input() siteKey: any;
  public waf = new Waf(this.httpClient, new _9Anime(this.httpClient));

  constructor(
    private httpClient: HttpClient,
    private _9anime: _9Anime
  ) {}

  ngAfterViewInit() {
    const webview = document.querySelector('webview#wafView');
    webview.addEventListener('page-title-updated', (e: any) => {
      if(e.title.includes("Watch Anime Online")) this._9anime.isWafActive = false;
    });

    // Show the modal
    $("#wafModal").show();
  }

  async onResolve(token) {
    //let resp = await this.waf.submitWaf(token);
    
    //this._9anime.isWafActive = false;
  }

}
