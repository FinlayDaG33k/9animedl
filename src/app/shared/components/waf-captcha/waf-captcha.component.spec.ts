import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WafCaptchaComponent } from './waf-captcha.component';

describe('WafCaptchaComponent', () => {
  let component: WafCaptchaComponent;
  let fixture: ComponentFixture<WafCaptchaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WafCaptchaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WafCaptchaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
