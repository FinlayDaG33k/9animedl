import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BsModalService, ModalDirective, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder } from '@angular/forms';
import { faTimes, faUser } from '@fortawesome/free-solid-svg-icons';

import { UserService } from '../../../user.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})

export class LoginModalComponent implements OnInit {
  @ViewChild('loginModal', {static: true}) modal: any;
  modalRef: BsModalRef;
  loginForm;

  faTimes = faTimes;
  faUser = faUser;

  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {
    this.loginForm = this.formBuilder.group({
      username: '',
      password: '',
      remember: false
    });
  }

  ngOnInit() {
    
  }

  public openModal() {
    this.modalRef = this.modalService.show(this.modal, {
      backdrop: true,
      class: 'modal-lg',
      ignoreBackdropClick: false
    });
  }

  async onSubmit(loginData) {
    let resp = await this.userService.login(loginData);
    if(!resp['success']) {
      this.userService.loginError = resp['message'];
    }

    this.userService.loginError = "";
    this.userService.getDetails();
    this.modalRef.hide();
  }
}
