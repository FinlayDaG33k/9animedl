import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { RecaptchaModule } from 'ng-recaptcha';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { PageNotFoundComponent } from './components/';
import { WebviewDirective } from './directives/';
import { AnimeCardComponent } from './components/anime-card/anime-card.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { WafCaptchaComponent } from './components/waf-captcha/waf-captcha.component';
import { NoResultsComponent } from './components/no-results/no-results.component';
import { DonateModalComponent } from './components/donate-modal/donate-modal.component';
import { LoginModalComponent } from './components/login-modal/login-modal.component';
import { BannedComponent } from './components/banned/banned.component';

import { UiSwitchModule } from 'ngx-toggle-switch';



@NgModule({
  declarations: [PageNotFoundComponent, WebviewDirective, AnimeCardComponent, LoadingSpinnerComponent, WafCaptchaComponent, NoResultsComponent, DonateModalComponent, LoginModalComponent, BannedComponent],
  imports: [CommonModule, TranslateModule, RecaptchaModule, UiSwitchModule, FontAwesomeModule, FormsModule, ReactiveFormsModule],
  exports: [TranslateModule, WebviewDirective, AnimeCardComponent, LoadingSpinnerComponent, WafCaptchaComponent, RecaptchaModule, NoResultsComponent, DonateModalComponent, LoginModalComponent, UiSwitchModule, BannedComponent]
})
export class SharedModule {}
