import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { StatusHandler } from '../util/StatusHandler';

@Injectable({
  providedIn: 'root'
})

export class InterceptorService implements HttpInterceptor{
  statusHandler: StatusHandler = new StatusHandler();

  constructor() { }

  handleError(error: HttpErrorResponse){
    let status = this.statusHandler.handle(error);
    return of(status);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
    return next.handle(req)
      .pipe(
        catchError((error, caught) => {
          this.handleError(error)
          return of(error);
        }) as any
      );
  }
}