import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DiscoverComponent } from './discover.component';

const routes: Routes = [
  {
    path: 'discover',
    component: DiscoverComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscoverRoutingModule {}
