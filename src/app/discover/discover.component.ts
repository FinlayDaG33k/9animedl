import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';

import { AppComponent } from '../app.component';

import { _9Anime } from '../util/_9Anime.util';

import $ from 'jquery';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss']
})

export class DiscoverComponent implements OnInit {
  series = [];
  page = 1;

  constructor(
    private _9anime: _9Anime,
    private router: Router,
    public appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.appComponent.isBusy = true;
    this._9anime.discoverAnime(this.page++).then((res) => {
      if(!res.error) {
        this.series = this.series.concat(res.data);
      } else {
        if(res.internal) {
          this.appComponent.notifier.error("Oops", `It appears something went wrong while trying to load series.`);
        } else {
          this.appComponent.notifier.error("Oops", `It appears something went wrong while trying to load series.\r\nThis error cannot be solved by the developers of 9AnimeDl.`);
        }
      }
      this.appComponent.isBusy = false;
    });

    let self = this;
    let main = $(`main`)[2];
    main.addEventListener("scroll", function(e) {
      if(main.scrollTop == (main.scrollHeight - main.clientHeight)) {
        self._9anime.discoverAnime(self.page).then((res) => {
          if(!res.error) {
            self.series = self.series.concat(res.data);
            self.page++;
          } else {
            if(res.internal) {
              this.appComponent.notifier.error("Oops", `It appears something went wrong while trying to load series.`);
            } else {
              this.appComponent.notifier.error("Oops", `It appears something went wrong while trying to load series.\r\nThis error cannot be solved by the developers of 9AnimeDl.`);
            }
          }
        })
      }
    });
  }
}
