import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';

import { AppComponent } from '../app.component';

import { _9Anime } from '../util/_9Anime.util';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
  series: any;

  constructor(
    public _9anime: _9Anime,
    private router: Router,
    public appComponent: AppComponent
  ) {
    try {
      this.series = this.router.getCurrentNavigation().extras.state.series;
    } catch(e) {
      this.appComponent.notifier.error("Oh boii", `Whoops, something went wrong while searching...\r\nSending you back to the discovery....`);
      this.router.navigate(['discover']);
    }
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.appComponent.isBusy = false;
  }
}
