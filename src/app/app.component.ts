import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ElectronService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { Router } from '@angular/router';

import { _9Anime } from './util/_9Anime.util';
import { Cache } from './util/Cache.util';
import { faSearch, faTimes, faMinus, faMinusSquare, faPlusSquare, faCodeBranch, faDonate, faInfo, faCommentAlt, faUser } from '@fortawesome/free-solid-svg-icons';
import { app } from 'electron';
import { DownloadTabService } from './download-tab.service';
import { UserService } from './user.service';
import { LoginModalComponent } from './shared/components/login-modal/login-modal.component';
import { Notifier } from './util/Notifier.util';
import { Logger } from './util/Logger.util';

import { SharedModule } from './shared/shared.module';

const win = require('electron').remote.getCurrentWindow();
const { shell } = require('electron');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(LoginModalComponent, {static: false}) private loginModalComponent:LoginModalComponent;
  logger = new Logger();
  searchTimeout = null;
  isBusy = true;
  nagDonation = false;
  isMaximized = win.isMaximized();
  downloads = [];

  faSearch = faSearch;
  faTimes = faTimes;
  faMinus = faMinus;
  faMinusSquare = faMinusSquare;
  faPlusSquare = faPlusSquare;
  faCodeBranch = faCodeBranch;
  faDonate = faDonate;
  faInfo = faInfo;
  faCommentAlt = faCommentAlt;
  faUser = faUser;

  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,
    public _9anime: _9Anime,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    public downloadTabService: DownloadTabService,
    public userService: UserService,
    public sharedModule: SharedModule,
    public notifier: Notifier
  ) {
    translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);

    if (electronService.isElectron) {
      console.log(process.env);
      console.log('Mode electron');
      console.log('Electron ipcRenderer', electronService.ipcRenderer);
      console.log('NodeJS childProcess', electronService.childProcess);
    } else {
      console.log('Mode web');
    }

    this.logger.info(`Starting 9AnimeDl - version ${require('electron').remote.app.getVersion()}`);

    let self = this;
    win.on('maximize', () => {
      self.isMaximized = true;
      this.cdRef.detectChanges();
    });

    win.on('unmaximize', () => {
      self.isMaximized = false;
      this.cdRef.detectChanges();
    });

    self.downloads = this.downloadTabService.getAllDownloads();
  }

  ngOnInit() {
    this.userService.getDetails();
  }

  public openTab(index: number) {
    this.downloadTabService.openTab(index);
  }

  public searchChange(val) {
    // Clear the previous timeout
    clearTimeout(this.searchTimeout);

    // Set a new timeout
    this.searchTimeout = setTimeout(() => {
      // Show the loading page
      this.isBusy = true;

      // Make a quick search
      this._9anime.searchAnime(val).then((res) => {
        this.router.navigate(['search'], { 
          state: { 
            series: res.data
          } 
        });
        this.isBusy = false;
      });
    }, 300);
  }

  public doDonate() {
    shell.openExternal('https://ko-fi.com/finlaydag33k');
  }

  public maximizeWindow() {
    win.maximize();
  }

  public unmaximizeWindow() {
    win.unmaximize();
  }

  public minimizeWindow() {
    win.minimize();
  }

  public exitApp() {
    // Remove cache before we exit
    let cache = new Cache();
    Object
      .entries(localStorage)
      .map(x => x[0])
      .filter(x => x.substring(0,15)=="discovery cache")
      .map(x => cache.remove(x));

    win.close();
  }

  public openRepo() {
    shell.openExternal('https://www.gitlab.com/finlaydag33k/9animedl');
  }

  public sendFeedback() {
    shell.openExternal('https://www.reddit.com/r/finlaydag33k');
  }
}
