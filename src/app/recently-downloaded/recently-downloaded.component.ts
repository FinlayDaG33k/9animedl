import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';

import { AppComponent } from '../app.component';

import { _9Anime } from '../util/_9Anime.util';
import { IndexedDb } from '../util/IndexedDb.util';

import $ from 'jquery';

@Component({
  selector: 'app-recently-downloaded',
  templateUrl: './recently-downloaded.component.html',
  styleUrls: ['./recently-downloaded.component.scss']
})

export class RecentlyDownloadedComponent implements OnInit {
  series = [];
  page = 1;
  recentDb = new IndexedDb();

  constructor(
    private _9anime: _9Anime,
    private router: Router,
    public appComponent: AppComponent
  ) {
    var self = this;
  }

  async ngOnInit() {
    this.appComponent.isBusy = true;

    // Open the db connection
    await this.recentDb.open("recently downloaded", 1, function(evt) {
      var db = evt.target.result;
      db.store = db.createObjectStore("serie", {keyPath: "name"});
    });

    // Get all the recent downloads
    // Sort them by the datestamp
    let res = await this.recentDb.get("serie");
    res = res.sort((a,b) => {
      if(a['date'] < b['date']) return 1;
      if(a['date'] > b['date']) return -1;
      return 0;
    });
    this.series = this.series.concat(res);
   
    this.appComponent.isBusy = false;
  }
}
