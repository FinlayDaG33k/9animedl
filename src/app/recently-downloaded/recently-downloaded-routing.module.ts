import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RecentlyDownloadedComponent } from './recently-downloaded.component';

const routes: Routes = [
  {
    path: 'discover',
    component: RecentlyDownloadedComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecentlyDownloadedRoutingModule {}
