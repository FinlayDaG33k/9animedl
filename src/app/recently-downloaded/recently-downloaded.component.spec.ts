import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentlyDownloadedComponent } from './recently-downloaded.component';
import { TranslateModule } from '@ngx-translate/core';

describe('RecentlyDownloadedComponent', () => {
  let component: RecentlyDownloadedComponent;
  let fixture: ComponentFixture<RecentlyDownloadedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecentlyDownloadedComponent],
      imports: [TranslateModule.forRoot()]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentlyDownloadedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
