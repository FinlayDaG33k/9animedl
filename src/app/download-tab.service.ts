import { Injectable } from '@angular/core';
import { Time } from './util/Time.util';
import { Router } from '@angular/router';
import { SeriesDB } from './util/SeriesDB.util';

export interface Download {
  serie: any;
  isInitialized?: boolean;
  isDownloading?: boolean;
  episodes?: Array<any>;
  promises?: Array<any>;
  downloadDir?: string;
  fileExample?: string;
  animeName?: string;
  season?: any;
}

@Injectable({
  providedIn: 'root'
})

export class DownloadTabService {
  downloads: Download[] = [];

  constructor(
    private router: Router
  ){}

  /**
   * Returns a list of all of the current downloads
   */
  getAllDownloads(): Download[] {
    return this.downloads;
  }

  /**
   * Returns a download at a specific index
   * 
   * @returns Download The download object
   */
  getDownload(index: number): Download {
    return this.downloads[index];
  }

  /**
   * Adds a download to the download list
   * 
   * @returns Number The index of the pushed item
   */
  addDownload(serie: any): Number {
    // Make sure we don't already have this anime in the list
    let downloads = this.downloads.find(download => download.serie.name == serie.name);
    if(downloads) {
      return;
    }

    const download:Download = {
      serie: serie
    };
    return this.downloads.push(download) - 1;
  }

  openTab(index: number) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.navigate(['download'], { 
      state: { 
        index: index
      } 
    });
  }

  searchDownload(name: string): number {
    return this.downloads.findIndex(download => download.serie.name == name);
  }
}
